import typeDefs from '../typeDefs';

const neo4j = require('neo4j-driver');
const uri = "bolt://localhost:7474";
const user = "neo4j";
const password = "1234";
console.log(typeDefs)
const driver = neo4j.driver(uri, neo4j.auth.basic(user, password), {
  maxConnectionLifetime: 3 * 60 * 60 * 1000, // 3 hours
  maxConnectionPoolSize: 50,
  connectionAcquisitionTimeout: 2 * 60 * 1000, // 120 seconds
  disableLosslessIntegers: true
});
const session = driver.session();
const executeCypherQuery = (statement, params = {}) => {
  try {
    const result = session.run(statement, params);
    session.close();
    return result;
  } catch (error) {
    throw error; // we are logging this error at the time of calling this method
  }
}

export default executeCypherQuery;