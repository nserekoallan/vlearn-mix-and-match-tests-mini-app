const { gql } = require('apollo-server');
module.exports = gql`
type Query{
    questions: [Question]
    question: Question
}

type Mutation{
    createdQuestion(input: createQuestionInput): Question
    deleteQuestion(id: Int!): Boolean
}

input createQuestionInput{
    qn_name: String!
    qn_answer: String!
    category: String
}

type Question{
    qn_name: String!
    qn_answer: String!
    category: Category! @relationship(type:"BELONGS_TO", direction: OUT)
}
`