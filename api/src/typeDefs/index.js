const userSchema = require('./user');
const defaultSchema = require('./default');
const testSchema = require('./test');
const lessonSchema = require('./lesson');
const questionSchema = require('./question');
const categorySchema = require('./category');
module.exports = [ categorySchema, lessonSchema, questionSchema, testSchema, userSchema, defaultSchema];
