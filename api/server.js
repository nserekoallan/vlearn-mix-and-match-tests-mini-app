const { ApolloServer, gql} = require('apollo-server');
const { Neo4jGraphQL } = require('@neo4j/graphql');
const neo4j = require('neo4j-driver');
require("dotenv").config();

/** Define types for the different entities */
const typeDefs = gql`
type User {
    id: ID! @id
    first_name: String
    last_name: String
    email: String!
    user_type: String
    createdAt: String
    updatedAt: String
}
type Test{
    id: ID! @id
    name: String!
    questions: [Question!]! @relationship(type:"HAS" direction: OUT) 
}

type Question{
    id: ID! @id
    qn_name: String!
    qn_answer: String!
    category: Category! @relationship(type:"BELONGS_TO", direction: OUT)
}
type Lesson{
    id: ID! @id
    name: String
    description: String
    level: String
    topic: String
    tests:[Test!]! @relationship(type:"HAS", direction: OUT)
}

type Category{
    id: ID @id
    name: String!
    description: String
}
`;
const driver = neo4j.driver(process.env.NEO4J_URI, neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD), {
    maxConnectionLifetime: 3 * 60 * 60 * 1000, // 3 hours
    maxConnectionPoolSize: 50,
    connectionAcquisitionTimeout: 2 * 60 * 1000, // 120 seconds
    disableLosslessIntegers: true
  });

const neoSchema = new Neo4jGraphQL({typeDefs, driver});

neoSchema.getSchema().then((schema)=>{
    const server = new ApolloServer({
        schema: schema
    });

    server.listen().then(({ url }) => {
        console.log(`🚀 Server ready at ${url}`);
    });
})

