import { BrowserRouter, Routes, Route }  from 'react-router-dom';
import Navbar1 from './components/Navbar1';
import Footer from './components/Footer';
import CreateQuestion from './components/questions/CreateQuestion';
import Questions from './components/questions/AllQuestions';

function App() {
  return (
    <> 
      <BrowserRouter>
        <Navbar1 />
        <Routes>
          <Route path='/questions/create' element={<CreateQuestion/>}></Route>
          <Route path='/questions' element={<Questions/>}></Route>
        </Routes>        
        <Footer />
      </BrowserRouter>
    </>
  );
}

export default App;
