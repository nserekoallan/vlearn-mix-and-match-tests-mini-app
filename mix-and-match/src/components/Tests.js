import React from "react";
import { MDBListGroup, MDBListGroupItem } from 'mdb-react-ui-kit';
import Test from "./Test";

const Tests = () => {
    return ( 
        <>
            <div >
                <h4>Tests</h4>
                <MDBListGroup  className="overflow-auto" vertical verticalSize='lg' style={{height: 500, width: 1000}}>
                    <Test name="Maths" id="1"/>
                </MDBListGroup>
                </div>
        </>
     );
}
 
export default Tests;