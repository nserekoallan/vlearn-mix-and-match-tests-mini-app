import { MDBInput, MDBRow, MDBCol } from 'mdb-react-ui-kit';
import { Container } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import React, { useState } from 'react';
import QuestionFormInput from "./QuestionFormInput";
import {gql, useMutation, useQuery} from '@apollo/client';


/**Mutation to add new question */
// const ADD_QUESTION = gql`
// mutation createQuestions($input:{}) {
//     questions{
//         qn_answer
//         qn_name
//     }
// }
// `;

/**Mutation to add new category */

/**Query to fetch all available categories */

const Questions = () => {
    const [show, setShow] = useState(false);
    const [formInput, setFormInput] = useState([<QuestionFormInput/>])
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
    /**Handle add question mutation */

    return ( 
        <>
        <h3 style={{textAlign: 'center', marginTop: 80}}>Add questions</h3>
        <Container  style={{marginLeft: 50, marginTop: 0, marginBottom: 100, backgroundColor: 'white', padding: 20}}>  
        
        <Button variant="light" style={{width: 300, paddingRight: 20}} onClick={handleShow}>Add Category</Button>{' '}
        <Modal show={show} onHide={handleClose} animation={false}>
            <Modal.Header closeButton>
                <h5 style={{textAlign: 'center'}}>Add a category</h5>
            </Modal.Header>
            <Container>
                        <Form>
                            
                            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                <Form.Label>Category Name</Form.Label>
                                <Form.Control placeholder="Enter Category" as="textarea" rows={1} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                <Form.Label>Description</Form.Label>
                                <Form.Control placeholder="Enter text" as="textarea" rows={3} />
                            </Form.Group>
                            </Form>
            </Container>
            
            <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
                Close
            </Button>
            <Button variant="primary" onClick={handleClose}>
                Save Changes
            </Button>
            </Modal.Footer>
        </Modal>
        
        <div style={{marginLeft: 20, marginRight: 20, marginTop: 50, marginBottom: 50, width: 1000, backgroundColor: 'aliceblue', padding: 10, borderRadius: 10}}>
        
            {formInput}

            <Row>
            <Col>
                <Button variant="primary" style={{width: 300}} onClick={()=>{
                    const comp = [...formInput,<QuestionFormInput/>];
                    setFormInput(comp);
                }}>Add question</Button>
            </Col>
            <Col>
                <Button type="submit" variant="primary" style={{ width: 300}}>Submit</Button>{' '}
            </Col>
        </Row>
        </div>
        
        
        </Container>
        
        </>
    );
}
 
export default Questions;