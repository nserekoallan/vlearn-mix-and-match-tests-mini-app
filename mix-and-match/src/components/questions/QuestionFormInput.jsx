import {Form, Row, Col} from "react-bootstrap";


const QuestionFormInput = () =>{

    return (
        <div>

<Row>
<Col auto>
<Form.Group className="mb-4">
<Form.Label></Form.Label>
    <Form.Select aria-label="Default select example">
        <option>Select Category</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
    </Form.Select>
</Form.Group>
</Col>
<Col auto>
<Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
    <Form.Label></Form.Label>
    <Form.Control placeholder="Enter question" as="textarea" rows={1} />
</Form.Group>
</Col>
<Col auto>
<Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
    <Form.Label></Form.Label>
    <Form.Control placeholder="Enter answer" as="textarea" rows={1} />
</Form.Group>
</Col>
</Row>

        </div>
  
);
}

export default QuestionFormInput;