import React from "react";
import { MDBListGroupItem } from 'mdb-react-ui-kit';
import { useDrag } from 'react-dnd';

const Question = (
    props
) => {
    const [{isDragging}, dragRef] = useDrag({
        type: 'question',
        // eslint-disable-next-line no-restricted-globals
        item: {name},
        collect: (monitor)=>({
            isDragging: monitor.isDragging()
        })
    })
    return ( 
        <>
            <div className="question-card"  ref={dragRef} style={{marginLeft: 70, marginRight: 20, marginTop: 30, marginBottom: 10, width: 500, backgroundColor: 'aliceblue', padding: 10, borderRadius: 10}}>
                <MDBListGroupItem>{props.name}</MDBListGroupItem>
                {isDragging && <p style={{color:"green"}}>'Add me to your Test'</p>}
            </div>
        </>
     );
}
 
export default Question;