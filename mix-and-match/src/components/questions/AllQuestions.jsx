import React from "react";
import { MDBListGroup} from 'mdb-react-ui-kit';
import Question from "./Question";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { useQuery, gql } from "@apollo/client";

const GET_QUESTIONS = gql`
query Tests {
  questions {
    qn_name
    qn_answer
    }
    }`;


const Questions = () => {
    const {loading, error, data} = useQuery(GET_QUESTIONS);
    if(loading){
        return <p>Loading...</p>
    }
    if(error) return <p>Error: {error.message}</p>

    return ( 
        <DndProvider backend={HTML5Backend}>
            <div>
                <h4>Questions</h4>
                <MDBListGroup  className="overflow-auto" vertical verticalSize='lg' style={{height: 500, width: 1000}}>
                        {data.questions.map(question => <Question name={question.qn_name}/>)}
                </MDBListGroup>
                </div>
        </DndProvider>
     );
}
 
export default Questions;