import React from "react";
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.css';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const Navbar1 = () => {
    return ( 
        <div>
            <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand href="/questions">Questions</Navbar.Brand>
        <Nav.Link href="/questions/create">Add Question</Nav.Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/tests">Tests</Nav.Link>
            <Nav.Link href="/create_form">Create</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
        </div>
     );
}
 
export default Navbar1;