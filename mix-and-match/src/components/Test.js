import React from "react";
import { MDBListGroupItem } from 'mdb-react-ui-kit';

// const TestStore = [
//     "What is SST?",
//     "1 + 1",
//     "Define plateu"
// ];

const Test = (
    props
) => {
    return ( 
        <>
            <div  style={{marginLeft: 20, marginRight: 20, marginTop: 50, marginBottom: 50, width: 700, backgroundColor: 'aliceblue', padding: 10, borderRadius: 10}}>
                <MDBListGroupItem href={`/${props.id}`}>{props.name}</MDBListGroupItem>
            </div>
        </>
     );
}
 
export default Test;