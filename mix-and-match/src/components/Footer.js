import React from "react";
import { MDBFooter, MDBContainer, MDBCol, MDBIcon } from 'mdb-react-ui-kit';


const Footer = () => {
    return ( 
        <div>
            <MDBFooter bgColor='light' className='text-center text-lg-start text-muted'>
      <section className='d-flex justify-content-center justify-content-lg-between p-4 border-bottom'>
      </section>

      <section className=''>
        <MDBContainer className='text-center text-md-start mt-5'>
            <MDBCol md="3" lg="4" xl="3" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>
                <MDBIcon icon="gem" className="me-3" />
Vlearn    Mix and Match          </h6>
              <p>
                
              </p>
            </MDBCol>

        </MDBContainer>
      </section>

    
    </MDBFooter>
        </div>
     );
}
 
export default Footer;